/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package device.interfaces;

/**
 *
 * @author invad
 */
public interface IStateConfiguration {
    public int TurnOn();
    public int TurnOff();
}
