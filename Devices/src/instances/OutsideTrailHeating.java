package instances;


import device.interfaces.IGetDeviceInfo;
import device.interfaces.IStateConfiguration;
import device.interfaces.ITemperatureConfiguration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author invad
 */
public class OutsideTrailHeating implements ITemperatureConfiguration,IStateConfiguration, IGetDeviceInfo {
    
    private boolean _isOn;
    private int _temperature;
    
    public OutsideTrailHeating()
    {
        _isOn = false;
        _temperature=50;
    }
    @Override
    public int IncreaseTemperature() {
        if (_isOn)
        {
            _temperature++;
            return _temperature;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int DecreaseTemperature() {
        if (_isOn){
            _temperature--;
            return _temperature;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int SetTemperature(int temperature) {
        if (_isOn){
            _temperature = temperature;
            return _temperature;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int TurnOn() {
        if (!_isOn){
            _isOn = true;
            return 1;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int TurnOff() {
        if (_isOn){
            _isOn = false;
            return 0;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public boolean GetState() {
        return _isOn;
    }

    @Override
    public String GetDeviceInfo() {
        if (_isOn){
            return "Outside trail heating is on and its working with temperature: "+_temperature + " C";
        }
        else {
            return "Outside trail heating is off";
        }
    }
    
}
