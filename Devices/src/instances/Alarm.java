package instances;


import device.interfaces.IGetDeviceInfo;
import device.interfaces.IStateConfiguration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author invad
 */
public class Alarm implements IGetDeviceInfo,IStateConfiguration{
    public boolean _isOn;
    public boolean _isBlocked;
    
    public Alarm(){
        _isOn = false;
        _isBlocked = false;
    }

    @Override
    public boolean GetState() {
        return _isOn;
    }

    @Override
    public String GetDeviceInfo() {
        if (_isOn){
            String state ="";
            if (_isBlocked)
                state ="blocked";
            else
                state ="unblocked";
            return "Alarm is on and house is "+state;
        }
        return "Alarm is off";
    }

    @Override
    public int TurnOn() {
        if (!_isOn){
            _isOn = true;
            return 1;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int TurnOff() {
        if (_isOn){
            _isOn = false;
            return 0;
        }
        else {
            throw new IllegalStateException();
        }
    }
    
    public int BlockHouse(){
        if (_isOn){
            _isBlocked=true;
            return 1;
        }
        else {
            throw new IllegalStateException();
        }
    }
    
    public int UnblockHouse(String code){
        if (_isOn){
            if (code=="1892a"){
                _isBlocked = false;
                return 1;
            }
            else 
                return 0;                            
        }
        else {
            throw new IllegalStateException();
        }
    }
    
    public boolean IsHouseBlocked(){
        if (_isOn)
            return !_isBlocked;
        else
            throw new IllegalStateException();
    }
}
