package instances;


import device.interfaces.IGetDeviceInfo;
import device.interfaces.IStateConfiguration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author invad
 */
public class Gate implements IStateConfiguration, IGetDeviceInfo {

    private boolean _isOn;
    private boolean _isOpen;
    
    public Gate(){
        _isOn=false;
        _isOpen=false;
    }
    
    @Override
    public int TurnOn() {
        if (!_isOn){
            _isOn = true;
            return 1;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int TurnOff() {
        if (_isOn){
            _isOn = false;
            return 0;
        }
        else {
            throw new IllegalStateException();
        }
    }
    
    public int OpenGate(){
        if (!_isOpen){
            _isOpen = true;
            return 1;
        }
        else {
           throw new IllegalStateException(); 
        }
    }
    
    public int CloseGate(){
        if(_isOpen){
            _isOpen = false;
            return 0;
        }
        else {
            throw new IllegalStateException();
        }
    }

    @Override
    public boolean GetState() {
        return _isOn;
    }

    @Override
    public String GetDeviceInfo() {
        if (_isOn){
            String state="";
            if (_isOpen)
                state="open";
            else 
                state="closed";
            return "Gates is working and its "+state;
        }
        else {
            return "Gates is not working";
        }
    }
    
}
