/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import device.interfaces.IGetDeviceInfo;
import device.interfaces.IStateConfiguration;
import instances.Alarm;
import instances.Conditioner;
import instances.Gate;
import instances.InsideHeating;
import instances.OutsideTrailHeating;



/**
 *
 * @author invad
 */
public class House {
    public Alarm _alarm;
    public Gate _gate;
    public OutsideTrailHeating _outsideTrailHeating;
    public InsideHeating _insideHeating;
    public Conditioner _conditioner;
    
    public void AddAlarm()
    {
        _alarm = new Alarm();
    }
    
    public void RemoveAlarm(){
        _alarm = null;
    }
    
    public void AddGate(){
        _gate = new Gate();
    }
    
    public void RemoveGate(){
        _gate = null;
    }
    
    public void AddOutsideHeating()
    {
        _outsideTrailHeating = new OutsideTrailHeating();
    }
    
    public void RemoveOutsideHeating(){
        _outsideTrailHeating = null;
    }
    
    public void AddInsideheating(){
        _insideHeating = new InsideHeating();
    }
    
    public void RemoveInsideHeating(){
        _insideHeating = null;
    }
    
    public void AddConditioner(){
        _conditioner = new Conditioner();
    }
    
    public void RemoveConditioner(){
        _conditioner = null;
    }
    
    public int TurnDeviceOn(IStateConfiguration device)
    {
        if (device !=null)
        return device.TurnOn();
        
        return 0;
    }
    
    public int TurnDeviceOff(IStateConfiguration device){
        if(device !=null)
        return device.TurnOff();
        
        return 0;
    }
    
    public String GetDeviceInfo(IGetDeviceInfo device){
        if (device!=null)
                return device.GetDeviceInfo();
        
        return "";
    }
    
    public int BlockHouse(){
        if (_alarm!=null)
            return _alarm.BlockHouse();
        return 0;
    }
    
    public int UnblockHouse(String code) {
        if(_alarm!=null)
            return _alarm.UnblockHouse(code);
        
        return 0;
    }
    
}
