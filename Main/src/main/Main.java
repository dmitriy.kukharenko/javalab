/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author invad
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        House house = new House();
        
        house.AddAlarm();
        house.AddConditioner();
        house.AddGate();
        house.AddOutsideHeating();
        house.AddInsideheating();
        
        System.out.println("Your house was configured");
        System.out.println("These are default states:");
        System.out.println(house.GetDeviceInfo(house._alarm));
        System.out.println(house.GetDeviceInfo(house._conditioner));
        System.out.println(house.GetDeviceInfo(house._gate));
        System.out.println(house.GetDeviceInfo(house._outsideTrailHeating));
        System.out.println(house.GetDeviceInfo(house._insideHeating));
        System.out.println("");
        System.out.println("Switch alaram, gates, and conditioner On");
        house._alarm.TurnOn();
        house._gate.TurnOn();
        house._conditioner.TurnOn();
        System.out.println("");
        System.out.println("Do some stuff:");
        System.out.println("");
        
        System.out.println("Ublocking the alarm with code 1892a");
        System.out.println(".....");
        
        
        house.UnblockHouse("1892a");
        System.out.println(house.GetDeviceInfo(house._alarm));
        
        System.out.println("Opening gates");
        System.out.println(".....");
        house._gate.OpenGate();
        System.out.println(house.GetDeviceInfo(house._gate));
        
        System.out.println("Switch conditioner mode");
        System.out.println("Set its temperature to 40 degrees");
        house._conditioner.SwitchMode(1);
        house._conditioner.SetTemperature(40);
        System.out.println(house.GetDeviceInfo(house._conditioner));
    }
    
}
